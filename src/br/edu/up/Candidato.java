package br.edu.up;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface Candidato extends Remote {
  
  String getNome() throws RemoteException;
  byte[] getFoto() throws RemoteException;
  List<Cedula> getVotos() throws RemoteException;

}