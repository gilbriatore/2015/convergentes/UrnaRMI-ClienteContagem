package br.edu.up;

import java.net.URL;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.List;
import java.util.ResourceBundle;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

public class ClienteContagemFx extends Application implements Initializable {

  private Urna urna;

  @FXML
  private ListView<Candidato> lstCandidatos;

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    try {
      urna = (Urna) Naming.lookup("rmi://localhost:9090/urna");
      efetuarContagem();
    } catch (Exception e) {
      Alert alert = new Alert(AlertType.ERROR);
      alert.setTitle("Aten��o!");
      alert.setHeaderText("N�o foi poss�vel conectar ao servidor!");
      alert.setContentText("Certifique-se de que ele j� foi iniciado!");
      alert.showAndWait();
      Platform.exit();
    }
  }

  private void efetuarContagem() {
    lstCandidatos.setItems(null);
    List<Candidato> candidatos;
    try {
      candidatos = urna.getCandidatosClassificados();
      ObservableList<Candidato> oLista = FXCollections.observableArrayList(candidatos);
      lstCandidatos.setItems(oLista);
      lstCandidatos.setCellFactory(new Callback<ListView<Candidato>, ListCell<Candidato>>() {
        @Override
        public ListCell<Candidato> call(ListView<Candidato> param) {
          ListCell<Candidato> cells = new ListCell<Candidato>() {
            @Override
            protected void updateItem(Candidato candidato, boolean empty) {
              super.updateItem(candidato, empty);
              if (candidato != null) {
                try {
                  setText(candidato.getNome() + ", Votos: " + candidato.getVotos().size());
                } catch (RemoteException e) {
                  e.printStackTrace();
                }
              }
            }
          };
          return cells;
        }
      });
    } catch (RemoteException e1) {
      e1.printStackTrace();
    }

    new Timeline(
        new KeyFrame(
              Duration.millis(2500), ae -> 
              efetuarContagem()
            )
        ).play();
  }

  public static void main(String[] args) {
    Application.launch(args);
  }

  @Override
  public void start(Stage tela) throws Exception {
    tela.setTitle("Urna Eletr�ncia RMI");
    URL url = ClienteContagemFx.class.getResource("TelaDeContagem.fxml");
    Pane painel = (Pane) FXMLLoader.load(url);
    tela.setScene(new Scene(painel));
    tela.show();
  }
}