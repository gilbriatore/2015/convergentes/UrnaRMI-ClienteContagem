package br.edu.up;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface Urna extends Remote {
  
  Cedula getCedula(String titulo) throws RemoteException;
  Candidato getCandidato(String codigo) throws RemoteException;
  void votar(Cedula cedula) throws RemoteException;
  List<Candidato> getCandidatosClassificados() throws RemoteException;
  
}