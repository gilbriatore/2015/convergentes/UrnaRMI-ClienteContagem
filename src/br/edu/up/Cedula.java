package br.edu.up;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Cedula extends Remote {

  void setCandidato(Candidato candidato) throws RemoteException;
 
}